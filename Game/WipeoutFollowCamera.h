#pragma once
#include <Camera.h>
class WipeoutFollowCamera : public Camera
{
public:
	WipeoutFollowCamera(GameObject * player);
	~WipeoutFollowCamera();

	void update(double deltaTimed, int, int, bool playing);
	glm::mat4 getLookAtMatrix(bool invertY, glm::vec3 pos);
protected:
	float y = 0.0f;

	glm::vec3 lookingAtConst = glm::vec3(0.0f, -5.0f, 0.0f);
	glm::vec3 targetAdjust = glm::vec3(0, 3, 0);

	glm::vec4 positionOffset = glm::vec4(0, 1.5f, -8, 0);

	float mouseSens = 500.0f;
	float maxX = 2.6f;

	float minX = -0.6f;


	float minHeightAtPoint = 0.2f;
	float radius = 8.0f;
};

