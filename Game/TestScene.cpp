#include "TestScene.h"
#include <Game.h>
#include <Skybox.h>
#include "WipeOutObjectFactory.h"
#include "PlayerShipMovement.h"
#include <SimpleCamera.h>
#include "SunLight.h"

TestScene::TestScene()
{
	name = "Test";
}


TestScene::~TestScene()
{
}

void TestScene::run()
{
	gameRefr->setPlayer(editor);
	gameRefr->setPlaying(true);
	gameRefr->setCaptureMouse(true);
}

void TestScene::load(Game* ref)
{
	gameRefr = ref;

	TestScene::loadInit();

	loadShipTest();

	loadPlane();

	Camera* staticCamera = new StaticLookAtCamera(glm::vec3(20, 8, 0), ship);

	gameRefr->setCamera(staticCamera);

	currLighting->addLight(new SunLight(glm::vec3(0.1f,0.9f,0.1f)));
}

void TestScene::loadInit()
{
	Scene::loadInit();

	GameObject* skyBox = new Skybox(0);

	addGameObject(skyBox);

	examiner = ObjExaminer::getInstance();

	objectMaker = WipeOutObjectFactory::getInstance();

	examiner->setDirectoryTexture("..\\..\\assets\\");
	examiner->setDirectoryObject("..\\..\\assets\\");
}

void TestScene::loadPlane()
{
	btStaticPlaneShape* plane = new btStaticPlaneShape(btVector3(0, 1, 0), -5);

	addHostlessCollider(plane, glm::mat4(1));
}

void TestScene::loadShipTest()
{
	glm::vec3 startPos = findWaypoint("ShipStart")->getPos() + glm::vec3(0, 15, 0);

	VAOData* shipData = new VAOData();

	examiner->setDirectoryObject("..\\..\\assets\\Ship1\\");
	examiner->setFile("ship2.obj");
	examiner->indexShapes(shipData);

	ship = objectMaker->createRaceShip(startPos, shipData, 0);
	ship->setRot(glm::vec3(0, 0, 0));
	addGameObject(ship);

	shipKeyboardMove = new PlayerShipMovement(ship);
	ship->addPrivateBehaviour(shipKeyboardMove);
}
