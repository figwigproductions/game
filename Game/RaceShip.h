#pragma once
#include <Object3D.h>
#include <Game.h>
#include <PhysicsComponent.h>
#include <glm-0.9.9.2\glm\glm.hpp>

struct HoverJet
{
	HoverJet(glm::vec3 pos)
	{

		posRelShip = pos;
	}


	Force getForce(double deltaTime)
	{	
		if (desiredForce.x > currForce.x)
		{
			currForce.x = (glm::min)(desiredForce.x, currForce.x + MAX_ADJUST);
		}
		else 
		{
			currForce.x = glm::max(desiredForce.x, currForce.x - MAX_ADJUST);
		}

		if (desiredForce.y > currForce.y)
		{
			currForce.y = glm::min(desiredForce.y, currForce.y + MAX_ADJUST);
		}
		else
		{
			currForce.y = glm::max(desiredForce.y, currForce.y - MAX_ADJUST);
		}

		if (desiredForce.z > currForce.z)
		{
			currForce.z = glm::min(desiredForce.z, currForce.z + MAX_ADJUST);
		}
		else
		{
			currForce.z = glm::max(desiredForce.z, currForce.z - MAX_ADJUST);
		}

		return Force(currForce, posRelShip);
	}

	void setDesiredForce(glm::vec3 s)
	{
		desiredForce = glm::max(glm::vec3(MIN_FORCE),glm::min(glm::vec3(MAX_FORCE), s));
	}

	void increaseDesiredForce(glm::vec3 s)
	{
		desiredForce = glm::max(glm::min(glm::vec3(MAX_FORCE), desiredForce+s),glm::vec3(MIN_FORCE));
	}

	void multiplyDesiredForce(float x)
	{
		desiredForce *= x;
	}

	glm::vec3 getPos()
	{
		return posRelShip;
	}
private:
	glm::vec3 currForce = glm::vec3( 0.0f);
	glm::vec3 desiredForce = glm::vec3(0.0f);

	//the following are per second
	float MIN_ADJUST = 15;
	float MAX_ADJUST = 200;
	glm::vec3 posRelShip;
	float MAX_FORCE = 3450;
	float MIN_FORCE = -200;

};



class RaceShip : public virtual Object3D
{
public:
	RaceShip(VAOData *model);
	~RaceShip();

	void update(double);
	void draw(RenderSettings*);

	bool active = true;

	void turnLeft();
	void turnRight();
	void forceForward(float speed = forwardForce);
	void forceBackward();
	void forceUp();
	void roll();


	void setup();
protected:

	PhysicsComponent* physics;
	//DebugCentre locs
	std::vector<int> forceWatchLocs;
	std::vector<int> jetHeightWatchLocs;

	glm::vec4 up = glm::vec4(0, 1, 0, 1);
	glm::vec4 forward = glm::vec4(0, 0, 1, 1);

	void hover(double x);
	void applyAirResistance(double x);

	float HOVER_TARGET_HEIGHT = 2.5f;
	float MAX_JET_FORCE = 4000.0f;//Newtons
	Game* game;


	HoverJet jets[4] = { HoverJet(glm::vec3(1, 0, -0.5f)), HoverJet(glm::vec3(1, 0, 0.5f)), HoverJet(glm::vec3(-1, 0, -0.5f)), HoverJet(glm::vec3(-1, 0, 0.5f)) };
	//fl, fr, bl, br, assuming 2 long and 1 wide



	//Hovering
	float hoverMultiplier = 10000.0f;
	float correctionMultiplier = 3.0f;
	float rollForce = 400.0f;
	float correctionalForce = 2500.0f;


	float AIR_RESISTANCE_MULTIPLIER = 200.2f;

	//Movement
	static float forwardForce;

	glm::vec4 rotForce = glm::vec4(40000, 0, 0, 1);


	glm::mat4 currRotMat = glm::mat4(1);
};

