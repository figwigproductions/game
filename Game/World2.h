#pragma once
#include <Scene.h>
#include "WipeOutObjectFactory.h"
#include "RaceShip.h"
#include "KeyboardMove.h"
#include "PlayerShipMovement.h"

class World2 : public Scene
{
public:
	World2();
	~World2();

	void load(Game* refr);
	void run();

private:



	void loadInit();

	GameObject* editor;

	void loadWorld();

	void loadWater();

	void loadEditor();

	void loadCamera();

	void loadLighting();

	Camera* editorCamera = nullptr;
	Camera* playerCamera = nullptr;

	void loadShip();

	void loadAiShip();

	VAOData* shipData = nullptr;

	WipeOutObjectFactory* objectMaker = nullptr;
	RaceShip* ship;

	KeyboardMove* cubeKeyboardMove;
	PlayerShipMovement* shipKeyboardMove;
};

