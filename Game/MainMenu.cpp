#include "MainMenu.h"
#include <LightingState.h>
#include <Config.h>
#include <Game.h>
#include <texture_loader.h>
#include <ImageRect.h>
#include <Skybox.h>
#include <Scenery.h>
#include <Rotator.h>
#include <WaterPlane.h>
#include <SimpleCamera.h>
#include <smallSpotLight.h>
#include <MoonLight.h>
#include <Fade.h>
#include <HorizonsUI.h>

MainMenu::MainMenu()
{
	name = "MainMenu";
}


MainMenu::~MainMenu()
{
}

void MainMenu::run()
{
	Fade* f = new Fade(FADE_FROM_BLACK);
	addPublicBehaviour(f);
	f->startFade(750);
}

void MainMenu::load(Game * refr)
{
	gameRefr = refr;

	Game::singleton()->getDisplayDetails()->aaAmount = 4;
	Game::singleton()->getDisplayDetails()->shadowMapSize = 3000.0f;

	loadInit();

	loadShip();

	loadWater();

	loadCamera();

	loadSky();

	loadUI();

	loadLighting();

	gameRefr->setConfigAll();
}

void MainMenu::loadInit()
{
	Scene::loadInit();

	examiner = ObjExaminer::getInstance();

	examiner->setDirectoryObject("..\\..\\assets\\Ship1\\");
	examiner->setDirectoryTexture("..\\..\\assets\\");

	debugSettings->lightSphere = false;

	gameRefr->setCaptureMouse(false);
}

void MainMenu::loadShip()
{
	examiner->setFile("ship2.obj");

	VAOData * shipData = new VAOData();

	examiner->indexShapes(shipData,"", false);

	Custom* ship = new Custom(shipData, glm::vec3(0, 2 , 0), glm::vec3(1.7f), glm::vec3(0));

	addGameObject(ship);

	ship->addPrivateBehaviour(new yRotator(ship, 0.0002f));
}

void MainMenu::loadWater()
{
	GLuint waterDudv = fiLoadTextureNormal("..\\..\\assets\\textures\\Water\\waterDUDV.png");
	GLuint waterNormal = fiLoadTextureNormal("..\\..\\assets\\textures\\Water\\waterNormalMap.png");

	WaterPlane * water = new WaterPlane(glm::vec2(40, 80));

	water->setPos(glm::vec3(0, 0, 0));

	addReflectiveGameObject(water);

	water->setDudv(waterDudv);

	water->setNormal(waterNormal);
}

void MainMenu::loadCamera()
{
	Camera * camera = new RotatingCamera();

	gameRefr->setCamera(camera);
}

void MainMenu::loadSky()
{
	Skybox * sky = new Skybox(1);

	sky->setPos(glm::vec3(0, -200, 0));

	addGameObject(sky);
}

void MainMenu::loadLighting()
{
	smallSpotLight * spot = new smallSpotLight(glm::vec3(10.0, 2.5, 2.5), glm::vec3(0, 10, 20));
	smallSpotLight * spot2 = new smallSpotLight(glm::vec3(0, 0, 3.5), glm::vec3(0, 10, -10));
	smallSpotLight * spot3 = new smallSpotLight(glm::vec3(3.5, 3.5, 3.5), glm::vec3(10, 10, -10));

	spot->coneDirection = glm::vec3(0.3f, -0.8, -1.0f);
	spot2->coneDirection = glm::vec3(0, -0.8, 1.0f);
	spot3->coneDirection = glm::vec3(0.3, -0.8, 1.0f);

	currLighting->addLight(spot);
	currLighting->addLight(spot2);
	currLighting->addLight(spot3);

	currLighting->addLight(new Moonlight(glm::vec3(0, 0.8f, 0.8f)));
}

void MainMenu::loadUI()
{
	TextureExaminer* texExam = TextureExaminer::singleton();

	texExam->setDirectory("..\\..\\assets\\");

	Texture t = texExam->getTexture("..\\..\\assets\\Textures\\UI\\Button.png");

	SceneOnButtonAction* level2Game = new SceneOnButtonAction("World");
	SceneOnButtonAction* testGame = new SceneOnButtonAction("Test");
	SceneOnButtonAction* level1Game = new SceneOnButtonAction("World2");

	Button* startButton = new Button(t, glm::vec3(0.35f), level1Game);
	Button* start2Button = new Button(t, glm::vec3(0.35f), level2Game);
	Button* testButton = new Button(t, glm::vec3(0.15f), testGame);

	Text* level1Text = new Text(gameRefr->getConfig().codeFontCharacters);
	Text* level2Text = new Text(gameRefr->getConfig().codeFontCharacters);
	Text* testText = new Text(gameRefr->getConfig().codeFontCharacters);

	level1Text->setText("Level 1");
	level2Text->setText("Level 2 (Less Polished)");
	testText->setText("Test Level");
	
	testText->setLocalScale(glm::vec3(1.8f));

	level1Text->centred = true;
	level2Text->centred = true;
	testText->centred = true;

	level1Text->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	level2Text->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	testText->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	level1Text->setParent(startButton);
	level2Text->setParent(start2Button);
	testText->setParent(testButton);

	addGUIObject(startButton);
	addGUIObject(start2Button);
	addGUIObject(testButton);
	addGUIObject(level1Text);
	addGUIObject(level2Text);
	addGUIObject(testText);

	addInputListener(startButton);
	addInputListener(start2Button);
	addInputListener(testButton);

	startButton->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	start2Button->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	testButton->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;

	startButton->setPos(glm::vec3(-220, -195, 0));
	start2Button->setPos(glm::vec3(220, -195, 0));

	testButton->setPos(glm::vec3(0, -310, 0));


	Text* title = new Text(gameRefr->getConfig().codeFontCharacters);

	title->setText("WipeOut");

	title->transform.setScale(glm::vec3(1.8f));

	title->centred = true;

	title->setPos(glm::vec3(0, 320, 0));

	addGUIObject(title);

	title->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;


	Text* message = new Text(gameRefr->getConfig().codeFontCharacters);
	Text* message2 = new Text(gameRefr->getConfig().codeFontCharacters);
	Text* message3 = new Text(gameRefr->getConfig().codeFontCharacters);
	Text* message4 = new Text(gameRefr->getConfig().codeFontCharacters);
	Text* message5 = new Text(gameRefr->getConfig().codeFontCharacters);

	message->setText("Welcome to the wipout implementation demo! Click the buttons below to start a level. Loading may take a second.");
	message2->setText("Once in game, use WASD to move, x to switch between racer and godmode, and space to go up.");
	message3->setText("If the mouse keeps glitching off the screen press f4 to show the mouse and again to hide it");
	message4->setText("F3 shows the debug menu and f5 shows just the fps");
	message5->setText("Enjoy!");

	message->transform.setScale(glm::vec3(0.25f));
	message2->transform.setScale(glm::vec3(0.25f));
	message3->transform.setScale(glm::vec3(0.25f));
	message4->transform.setScale(glm::vec3(0.25f));
	message5->transform.setScale(glm::vec3(0.2f));

	message->centred = true;
	message2->centred = true;
	message3->centred = true;
	message4->centred = true;
	message5->centred = true;

	message->setPos(glm::vec3(0, 250, 0));
	message2->setPos(glm::vec3(0, 235, 0));
	message3->setPos(glm::vec3(0, 220, 0));
	message4->setPos(glm::vec3(0, 205, 0));
	message5->setPos(glm::vec3(0, 190, 0));

	addGUIObject(message);
	addGUIObject(message2);
	addGUIObject(message3);
	addGUIObject(message4);
	addGUIObject(message5);

	message2->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	message3->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	message4->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	message5->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
	message->transform.anchorType = GUI_TRANSFORM_ANCHOR_CEN;
}


