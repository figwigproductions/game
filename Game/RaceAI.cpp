#include "RaceAI.h"
#include <Game.h>
#include <random>

RaceAI::RaceAI(RaceShip * newShip, Path* newPath)
{
	raceShip = newShip;
	path = newPath;
	currWaypoint = path->getWaypoint(index);

	//crossLoc = Game::singleton()->debugCentre->addWatch(0.0f, "AI Cross");
	//headingLoc = Game::singleton()->debugCentre->addWatch(0.0f, "AI heading");
	//distLoc = Game::singleton()->debugCentre->addWatch(0.0f, "Distance to waypoint");
	//dotLoc = Game::singleton()->debugCentre->addWatch(0.0f, "dot to waypoint");

	
	forwardForce += (std::rand() % 4000)-2000;

	angleFudge += ((std::rand() & 10) - 5.0f) / 100.0f;
	forwardsThreshold += ((std::rand() & 10) - 5.0f) / 100.0f;
}


RaceAI::~RaceAI()
{
}


void RaceAI::update(double)
{
	//Get the racer's position and the destination position
	glm::vec3 racerPos;
	glm::vec3 destPos;
	racerPos = target->getPos();
	racerPos.y = 0;
	destPos = currWaypoint->getPos();
	destPos.y = 0;

	//Calulate the racer's heading
	glm::vec4 heading = target->getRotMat() * glm::vec4(0, 0, 1, 1);
	glm::vec3 racerHeading = heading;

	//Calculate the heading towards the target
	glm::vec3 targetHeading = glm::normalize(racerPos - destPos);

	//Calculate the cross product based on this
	float crossProduct = (targetHeading.x * racerHeading.z) - (targetHeading.z * racerHeading.x);

	//and the dot of that direction
	float dot = glm::dot( targetHeading,racerHeading );

	//Initial force forward
	float go = forwardForce;

	//check if the ship is upright
	glm::vec4 upright = target->getRotMat() * glm::vec4(0, 1,1, 1);
	if (upright.y < 0.2f)
	{
		raceShip->roll();
		
	}

	//Left rights etc
	if (crossProduct> angleFudge)
	{
		raceShip->turnRight();
		go = forwardForce / 2.0f;
	}

	if (crossProduct < -angleFudge)
	{
		raceShip->turnLeft();
		go = forwardForce / 2.0f;
	}

	
	if (abs(crossProduct) < forwardsThreshold)
	{
		raceShip->forceForward(go);
	}

	float distToWP = glm::length(racerPos - destPos);
	if (distToWP < minimumRadius)
	{
		index++;
		currWaypoint = path->getWaypoint(index);
	}


	//Debug watches

	//Game::singleton()->debugCentre->updateWatch(crossLoc, crossProduct);
	//Game::singleton()->debugCentre->updateWatch(dotLoc, dot);
	//Game::singleton()->debugCentre->updateWatch(distLoc, distToWP);
	//Game::singleton()->debugCentre->updateWatch(headingLoc, racerHeading);
}