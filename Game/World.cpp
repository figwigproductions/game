#include "World.h"
#include <Skybox.h>
#include <Game.h>
#include <FirstPersonCamera.h>
#include <Scenery.h>
#include "WipeoutTerrainTextureData.h"
#include "SunLight.h"
#include <CheckPlayerHeight.h>
#include <WaterPlane.h>
#include "WipeOutObjectFactory.h"
#include <GenerateCube.h>
#include "NoClip.h"
#include "WipeoutFollowCamera.h"
#include "RaceAI.h"
#include <Path.h>

World::World()
{
	name = "World";
}


World::~World()
{
}

void World::run()
{
	gameRefr->setPlayer(editor);
	gameRefr->setPlaying(true);
	gameRefr->setCaptureMouse(true);

	gameRefr->setConfigAll();
}

void World::load(Game * thisGame)
{
	gameRefr = thisGame;

	World::loadInit();

	loadWorld();

	loadShip();

	loadEditor();

	loadCamera();

	loadLighting();

	loadAiShip();

	ship->addPrivateBehaviour(new NoClip(  cubeKeyboardMove, shipKeyboardMove, editorCamera, playerCamera));
}

void World::loadTextures()
{

}

void World::loadInit()
{
	Scene::loadInit();

	loadTextures();

	GameObject * skyBox = new Skybox(0);

	addGameObject(skyBox);

	examiner = ObjExaminer::getInstance();

	objectMaker = WipeOutObjectFactory::getInstance();

	examiner->changeObjectFactory(objectMaker);

	examiner->setDirectoryTexture("..\\..\\assets\\");
	examiner->setDirectoryObject("..\\..\\assets\\");
}

void World::loadWorld()
{
	//Load in the world obj file 
	examiner->setDirectoryObject("..\\..\\assets\\World\\");
	examiner->setFile("World.obj");

	SceneConfig sc;
	sc.fixInstancedObjectsOnTerrain = false;

	sc.addInstancible("BarrierSection_A");
	sc.addInstancible("BarrierSection_B");
	sc.tilingFactor = 200.0f;
	sc.texData = new WipeoutTerrainTextureData();
	examiner->indexScene(this, sc);

	loadWater();
}

void World::loadEditor()
{
	examiner->setDirectoryObject("..\\..\\assets\\Player\\");
	examiner->setFile("Cube.obj");

	VAOData *cube = new VAOData();

	examiner->indexShapes(cube);

	Custom *cube1 = new Custom(cube, findWaypoint("ShipStart")->getPos(), glm::vec3(0.02f), glm::vec3(0.0f, 0.0f, 0.0f));

	addGameObject(cube1);

	editor = cube1;
	cubeKeyboardMove = new KeyboardMove(gameRefr);
	editor->addPrivateBehaviour(cubeKeyboardMove);


	GenerateCube* genCube = new GenerateCube(cube, editor, this);
	editor->addPrivateBehaviour(genCube);

	addInputListener(genCube);
}

void World::loadCamera()
{
	editorCamera = new FirstPersonCamera(editor);
	playerCamera = new WipeoutFollowCamera(ship);
	gameRefr->setCamera(editorCamera);
}

void World::loadLighting()
{
	SunLight * sun = new SunLight(glm::vec3(0,0.8, 0.5));
	sun->castShadows = true;
	currLighting->addLight(sun);
}

void World::loadShip()
{
	glm::vec3 startPos = findWaypoint("ShipStart")->getPos() + glm::vec3(10,10,10);

	shipData = new VAOData();

	examiner->setDirectoryObject("..\\..\\assets\\Ship1\\");
	examiner->setFile("ship2.obj");
	examiner->indexShapes(shipData, "", false);

	ship = objectMaker->createRaceShip(startPos, shipData, 0);
	ship->setRot(glm::vec3(0,0, 0));
	addGameObject(ship);

	shipKeyboardMove = new PlayerShipMovement((RaceShip*)ship);
	ship->addPrivateBehaviour(shipKeyboardMove);
}

void World::loadWater()
{
	GLuint waterDUDV = fiLoadTextureNormal("..\\..\\assets\\textures\\Water\\waterDUDV.png");
	GLuint waterNormal = fiLoadTextureNormal("..\\..\\assets\\textures\\Water\\waterNormalMap.png");

	WaterPlane * water = new WaterPlane(glm::vec2(25));
	water->setDudv(waterDUDV);
	water->setNormal(waterNormal);

	addReflectiveGameObject(water);

	water->setPos(glm::vec3(0, 2.5f, 0));

	btStaticPlaneShape* planeCollider = new btStaticPlaneShape(btVector3(0, 1, 0), 2.0f);

	addHostlessCollider(planeCollider, glm::mat4(1));
}

void World::loadAiShip()
{
	glm::vec3 startPos = findWaypoint("ShipStart")->getPos() + glm::vec3(0, 0, 5);

	RaceShip* ship = objectMaker->createRaceShip(startPos, shipData, 0);
	ship->setRot(glm::vec3(0, 0, 0));
	addGameObject(ship);

	Path* path = new Path();

	path->generateFromSceneWithKeyword(this, "RacingLine");

	ship->addPrivateBehaviour(new RaceAI(ship, path));
}
