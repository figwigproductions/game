#pragma once
#include <PrivateBehaviour.h>
#include <PhysicsComponent.h>
class RaceShip;
class Game;
class PlayerShipMovement : public PrivateBehaviour
{
public:
	PlayerShipMovement(RaceShip * ship);
	~PlayerShipMovement();

	void update(double);

	void setup();

private:
	Game* gameRefr;

	RaceShip* ship;


};

