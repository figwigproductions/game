#include "WipeOutObjectFactory.h"
#include "RaceShip.h"
#include <PhysicsComponent.h>
#include <Scene.h>

WipeOutObjectFactory* WipeOutObjectFactory::thisPointer = nullptr;

WipeOutObjectFactory::WipeOutObjectFactory()
{
}


WipeOutObjectFactory::~WipeOutObjectFactory()
{
}

RaceShip * WipeOutObjectFactory::createRaceShip(glm::vec3 pos, VAOData * model, int type)
{
	RaceShip *ship = new RaceShip(model);
	ship->setPos(pos);

	PhysicsComponent* phys = new PhysicsComponent(ship, 1200);

	ship->addComponent(phys);

	ship->e = model->getExtents(0);

	ship->setScale(glm::vec3(0.6f));
	//ship->setVolume(makeComplexCollider(ship->getVAOData()));
	ship->setVolume(makeBoxCollider(ship));
	phys->setupBullet();
	phys->body->setDamping(0.65f, 0.9f);


	return ship;
}

WipeOutObjectFactory* WipeOutObjectFactory::getInstance()
{
	if (thisPointer == nullptr)
	{
		thisPointer = new WipeOutObjectFactory();
	}

	return thisPointer;
}

bool WipeOutObjectFactory::handleCustomPrefix(tinyobj::shape_t shape)
{
	ObjExaminer* examiner = ObjExaminer::getInstance();

	if ((shape.name.find("Track") != string::npos)||(shape.name.find("Barrier")!= string::npos))
	{
		VAOData* trackData = new VAOData();

		examiner->indexShapes(trackData, shape.name, false);

		addToTerrain(trackData);

		//Still want the track to be imported, so counted as not handled
		return false;
	}
	return false;
}
