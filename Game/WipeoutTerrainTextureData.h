#pragma once
#include <TerrainTextureData.h>
#include <texture_loader.h>
class WipeoutTerrainTextureData : public TerrainTextureData
{
public:
	WipeoutTerrainTextureData()
	{
		bases[0] = fiLoadTexture("..\\..\\assets\\Textures\\World\\grass01.jpg");
		normals[0] = fiLoadTextureNormal("..\\..\\assets\\Textures\\World\\grass01_n.jpg");
		specs[0] = fiLoadTexture("..\\..\\assets\\Textures\\World\\grass01_s.jpg");
		normalInvertY[0] = 1;

		bases[1] = fiLoadTexture("..\\..\\assets\\Textures\\World\\Sand01.jpg");
		normals[1] = fiLoadTextureNormal("..\\..\\assets\\Textures\\World\\Sand01_n.jpg");
		specs[1] = fiLoadTexture("..\\..\\assets\\Textures\\World\\Sand01_s.jpg");

		bases[2] = fiLoadTexture("..\\..\\assets\\Textures\\World\\dirt01.jpg");
		normals[2] = fiLoadTextureNormal("..\\..\\assets\\Textures\\World\\dirt01_n.jpg");
		specs[2] = fiLoadTexture("..\\..\\assets\\Textures\\World\\dirt01_s.jpg");

		splat = fiLoadTexture("..\\..\\assets\\Textures\\World\\splat.png");
	}
	~WipeoutTerrainTextureData() {}
};

