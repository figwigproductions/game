#pragma once
#include <ObjectFactory.h>
#include <ObjExaminer.h>
class RaceShip;
class Scene;
class WipeOutObjectFactory :public ObjectFactory
{
public:
	RaceShip * createRaceShip(glm::vec3 pos, VAOData* model, int type);

	static WipeOutObjectFactory* getInstance();
	 
	bool handleCustomPrefix(tinyobj::shape_t shape);


private:
	static WipeOutObjectFactory * thisPointer;
	WipeOutObjectFactory();
	~WipeOutObjectFactory();
};

