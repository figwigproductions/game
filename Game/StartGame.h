#pragma once
#include <GameObject.h>
#include <Game.h>
#include <Volume.h>
#include <PublicBehaviour.h>

class StartGame :public PublicBehaviour
{
public:
	StartGame(Game * refr) : gameRefr(refr) {}
	~StartGame();

	
	void update(double f);
	void draw(RenderSettings*) {}

	Volume * getVolume() { return nullptr; }
private:
	bool pressed =false;

	Game * gameRefr;
};

