#pragma once
#include <PrivateBehaviour.h>
class Camera;

class NoClip : public PrivateBehaviour
{
public:
	NoClip(PrivateBehaviour * , PrivateBehaviour * , Camera *, Camera * );
	~NoClip();

	void update(double);
private:
	bool  first = true;
	PrivateBehaviour * beh1;
	PrivateBehaviour * beh2;
	Camera * cam;
	Camera * cam2;

	double ticker = 0.0;
};

