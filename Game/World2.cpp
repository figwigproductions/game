#include "World2.h"
#include <Skybox.h>
#include <Game.h>
#include <FirstPersonCamera.h>
#include <Scenery.h>
#include "WipeoutTerrainTextureData.h"
#include "SunLight.h"
#include <CheckPlayerHeight.h>
#include <WaterPlane.h>
#include "WipeOutObjectFactory.h"
#include <GenerateCube.h>
#include "NoClip.h"
#include "WipeoutFollowCamera.h"
#include "RaceAI.h"
#include "KeyboardMove.h"
#include <Path.h>
#include "PlayerShipMovement.h"
#include <bullet/btBulletCollisionCommon.h>
#include <smallSpotLight.h>

World2::World2()
{
	name = "World2";
}


World2::~World2()
{
}

void World2::run()
{
	gameRefr->setPlayer(editor);
	gameRefr->setPlaying(true);
	gameRefr->setCaptureMouse(false);
	gameRefr->setCaptureMouse(true);

	gameRefr->setConfigAll();
	this;
}

void World2::load(Game* thisGame)
{
	gameRefr = thisGame;

	World2::loadInit();

	loadWorld();

	loadShip();

	loadEditor();

	loadCamera();

	loadLighting();

	loadAiShip();

	ship->addPrivateBehaviour(new NoClip( shipKeyboardMove, cubeKeyboardMove, playerCamera, editorCamera));
}

void World2::loadInit()
{
	Scene::loadInit();

	GameObject* skyBox = new Skybox(0);

	addGameObject(skyBox);

	examiner = ObjExaminer::getInstance();

	objectMaker = WipeOutObjectFactory::getInstance();

	examiner->changeObjectFactory(objectMaker);

	examiner->setDirectoryTexture("..\\..\\assets\\");
	examiner->setDirectoryObject("..\\..\\assets\\");
}

void World2::loadWorld()
{
	//Load in the world obj file 
	examiner->setDirectoryObject("..\\..\\assets\\World\\");
	examiner->setFile("World2.obj");

	SceneConfig sc;
	sc.fixInstancedObjectsOnTerrain = false;

	sc.addInstancible("BarrierSection_A");
	sc.tilingFactor = 200.0f;
	sc.texData = new WipeoutTerrainTextureData();
	examiner->indexScene(this, sc);

	loadWater();
}

void World2::loadEditor()
{
	examiner->setDirectoryObject("..\\..\\assets\\Player\\");
	examiner->setFile("Cube.obj");

	VAOData* cube = new VAOData();

	examiner->indexShapes(cube);

	Custom* cube1 = new Custom(cube, findWaypoint("ShipStart")->getPos(), glm::vec3(0.02f), glm::vec3(0.0f, 0.0f, 0.0f));

	addGameObject(cube1);

	editor = cube1;
	cubeKeyboardMove = new KeyboardMove(gameRefr);
	editor->addPrivateBehaviour(cubeKeyboardMove);


	GenerateCube* genCube = new GenerateCube(cube, editor, this);
	editor->addPrivateBehaviour(genCube);

	Game::singleton()->debugCentre->addObject(editor, "Editor Pos");

	addInputListener(genCube);
}

void World2::loadCamera()
{
	editorCamera = new FirstPersonCamera(editor);
	playerCamera = new WipeoutFollowCamera(ship);
	gameRefr->setCamera(playerCamera);
}

void World2::loadLighting()
{
	SunLight* sun = new SunLight(glm::vec3(0, 0.8, 0.5));
	sun->castShadows = true;
	currLighting->addLight(sun);

	smallSpotLight* amberLight1 = new smallSpotLight(glm::vec3(1.9f, 1.8f, 0.3f), glm::vec3(-5, 12, 0));
	smallSpotLight* amberLight2 = new smallSpotLight(glm::vec3(1.9f, 1.8f, 0.3f), glm::vec3(-5, 12, 100));
	smallSpotLight* amberLight3 = new smallSpotLight(glm::vec3(1.9f, 1.8f, 0.3f), glm::vec3(-5, 12, 190));
	
	amberLight1->coneAngle = 70;
	amberLight2->coneAngle = 70;
	amberLight3->coneAngle = 70;

	currLighting->addLight(amberLight1);
	currLighting->addLight(amberLight2);
	currLighting->addLight(amberLight3);
}

void World2::loadShip()
{
	glm::vec3 startPos = findWaypoint("ShipStart001")->getPos();

	shipData = new VAOData();

	examiner->setDirectoryObject("..\\..\\assets\\Ship1\\");
	examiner->setFile("ship2.obj");
	examiner->indexShapes(shipData, "", false);

	ship = objectMaker->createRaceShip(startPos, shipData, 0);
	ship->setRot(glm::vec3(0, 0, 0));
	addGameObject(ship);

	shipKeyboardMove = new PlayerShipMovement((RaceShip*)ship);
	ship->addPrivateBehaviour(shipKeyboardMove);
}

void World2::loadWater()
{
	GLuint waterDUDV = fiLoadTextureNormal("..\\..\\assets\\textures\\Water\\waterDUDV.png");
	GLuint waterNormal = fiLoadTextureNormal("..\\..\\assets\\textures\\Water\\waterNormalMap.png");

	WaterPlane* water = new WaterPlane(glm::vec2(50));
	water->setDudv(waterDUDV);
	water->setNormal(waterNormal);

	addReflectiveGameObject(water);

	water->setPos(glm::vec3(0, 3.5f, 0));

	btStaticPlaneShape* planeCollider = new btStaticPlaneShape(btVector3(0, 1, 0), 2.0f);


	addHostlessCollider(planeCollider, glm::mat4(1));
}

void World2::loadAiShip()
{
	glm::vec3 startPos1 = findWaypoint("ShipStart002")->getPos() + glm::vec3(0, 0, 5);
	glm::vec3 startPos2 = findWaypoint("ShipStart003")->getPos() + glm::vec3(0, 0, 5);
	glm::vec3 startPos3 = findWaypoint("ShipStart004")->getPos() + glm::vec3(0, 0, 5);

	RaceShip* ship1 = objectMaker->createRaceShip(startPos1, shipData, 0);
	RaceShip* ship2 = objectMaker->createRaceShip(startPos2, shipData, 0);
	RaceShip* ship3 = objectMaker->createRaceShip(startPos3, shipData, 0);

	ship1->setRot(glm::vec3(0, 0, 0));
	ship2->setRot(glm::vec3(0, 0, 0));
	ship3->setRot(glm::vec3(0, 0, 0));

	addGameObject(ship1);
	addGameObject(ship2);
	addGameObject(ship3);

	Path* path = new Path();

	path->generateFromSceneWithKeyword(this, "RacingLine");

	ship1->addPrivateBehaviour(new RaceAI(ship1, path));
	ship2->addPrivateBehaviour(new RaceAI(ship2, path));
	ship3->addPrivateBehaviour(new RaceAI(ship3, path));
}
