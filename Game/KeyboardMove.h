#pragma once
#include <PrivateBehaviour.h>
#include <keyState.h>
#include <glm-0.9.9.2\glm\glm.hpp>
#include <math.h>
class Game;
class KeyboardMove : public PrivateBehaviour
{
public:
	KeyboardMove(Game*);
	~KeyboardMove();
	void update(double);
	void setSpeed(float x) { speed = x; }
	void setMaxSpeed(float x) { maxSpeed = x; }

private:
	float speed = 0.004f;
	float maxSpeed = 0.028f;
	float decay = 0.991f;
	Game * gameRefr;

	Keystate currKeys;

	glm::vec3 additive = glm::vec3(0);
	glm::vec3 rotAdd = glm::vec3(0);
};

