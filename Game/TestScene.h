#pragma once
#include <Scene.h>
#include "RaceShip.h"
class WipeOutObjectFactory;
class PlayerShipMovement;


class TestScene : public Scene
{
public:
	TestScene();
	~TestScene();
	void run();
	void load(Game*);

private:
	void loadInit();
	void loadPlane();
	void loadShipTest();

	GameObject* editor = nullptr;

	WipeOutObjectFactory* objectMaker;

	RaceShip* ship;
	PlayerShipMovement* shipKeyboardMove;
};

