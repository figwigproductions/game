#version 330
uniform sampler2D textureImage;

// input packet

in packet{
	vec2 textureCoord;
} inputFragment;


// output packet
layout(location = 0) out vec4 fragmentColour;


void main(void) {

	vec3 gamma = vec3(1.0 / 2.2);
	vec4 tempFragmentColour = texture2D(textureImage, inputFragment.textureCoord);
	fragmentColour = vec4(pow(tempFragmentColour.xyz, gamma), tempFragmentColour.a);

}