#version 330
uniform sampler2D textureImage;
uniform float brightFactor;
// input packet

in packet{
	vec2 textureCoord;
} inputFragment;


// output packet
layout(location = 0) out vec4 fragmentColour;


void main(void) {
	vec4 tempColour = texture2D(textureImage, inputFragment.textureCoord);

	fragmentColour =tempColour * brightFactor;
}