#pragma once
#include <Light.h>

class SunLight : public Light 
{

public:

	SunLight(glm::vec3 pos)
	{
		position = glm::vec4(pos, 0.0f);
		attenuation = 0.01f;
		ambientCoefficient = 0.003f;
		intensities = glm::vec3(1.3f, 1.3f, 1.2f);
	}
};
