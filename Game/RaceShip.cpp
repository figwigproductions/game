#include "RaceShip.h"
#include <Helpers.h>

float RaceShip::forwardForce = 62500.0f;

RaceShip::RaceShip(VAOData * model) 
{
	dataVAO = model;
	game =Game::singleton();

	forceWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet1 force"));
	jetHeightWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet1 height"));
	forceWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet2 force"));
	jetHeightWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet2 height"));
	forceWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet3 force"));
	jetHeightWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet3 height"));
	forceWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet4 force"));
	jetHeightWatchLocs.push_back(game->debugCentre->addWatch(0.0f, "Jet4 height"));


}

void RaceShip::setup()
{
	physics = getComponent<PhysicsComponent*>();
}

RaceShip::~RaceShip()
{
}

void RaceShip::update(double x)
{
	Object3D::update(x);
	currRotMat = getRotMat();

	hover(x);

	//applyAirResistance(x);
	Game* game = Game::singleton();
	thisConfig;
	Object3D::lateUpdate(x);
}

void RaceShip::hover(double x)
{
	//get the model and rotation matricies
	glm::mat4 rotMat = getRotMat();
	glm::mat4 modelMat = getModelMat();

	//Get a raycast for the height
	RayCastResult r = game->getPhysicsEngine()->castRayDown(getPos());

	//Work out the height  relative to desired height
	float shipCurrHeight = getPos().y - (r.hitPos.y + HOVER_TARGET_HEIGHT);


	//Loop through each jet, calculate it's world and relative position, and use that to calculate a hovering and 
	// correctional force
	for (int jetIndex = 0; jetIndex < 4; jetIndex++)
	{
		//Ray cast at the jet's position
		glm::vec4 jetPos = glm::vec4(jets[jetIndex].getPos(), 1);
		glm::vec4 jetWorldPos = modelMat * jetPos;
		glm::vec4 jetRelPos = rotMat * jetPos;
		
		//update the force locs
		game->debugCentre->updateWatch(forceWatchLocs[jetIndex], jetWorldPos);

		//Cast a ray down from the jet
		RayCastResult r = game->getPhysicsEngine()->castRayDown(jetWorldPos);

		//Calculate the height of the jet relative to desired height
		float height = jetWorldPos.y - (r.hitPos.y + HOVER_TARGET_HEIGHT);

		// Tell the debug centre the height
		game->debugCentre->updateWatch(jetHeightWatchLocs[jetIndex], height);

		//Caluclate a force
		float force = physics->mass * 10;
		force -= height * hoverMultiplier;

		//If the jet height is higher than the centre of the ship, apply a downwards force at it's position. This is independant to the 
		//jet's main force
		if (height > shipCurrHeight)
		{
			float corrForce = (-correctionalForce * (height - shipCurrHeight)) * correctionMultiplier;
			physics->body->applyForce(convert(glm::vec3(0, corrForce, 0)), convert(jetRelPos));

		}

		//Set the desired force
		jets[jetIndex].setDesiredForce(glm::vec3(0, force / 4, 0));
	}
	

	Keystate k = game->getKeys();

	if (k & Keys::e)
	{
		// 0, 2 increase
		jets[0].increaseDesiredForce(glm::vec3(0,rollForce,0));
		jets[2].increaseDesiredForce(glm::vec3(0,rollForce,0));
	}

	if (k & Keys::q)
	{
		//1, 3 increase
		jets[1].increaseDesiredForce(glm::vec3(0, rollForce, 0));
		jets[3].increaseDesiredForce(glm::vec3(0, rollForce, 0));
	}

	for (int jetIndex = 0; jetIndex < 4; jetIndex++)
	{
		
		Force forceToApply = jets[jetIndex].getForce(x) * rotMat;

		physics->applyForce(forceToApply);

		//game->debugCentre->updateWatch(forceWatchLocs[jetIndex], forceToApply.magnitude);
	}

	if (shipCurrHeight > 20)
	{
		physics->body->applyCentralForce(btVector3(0, -1000, 0));
	}

}

void RaceShip::applyAirResistance(double x)
{
	glm::vec3 velocity = convert(physics->body->getLinearVelocity());
	//Check if the velocity returns as nan
	if (glm::all(glm::isnan(velocity)))
	{
		return;
	}

	glm::vec4 currForwards = getRotMat() * forward;

	//We're not using this for up and down
	velocity.y = 0;
	currForwards.y = 0;



	if (velocity.x == 0.0f && velocity.z == 0.0f)
	{
		return;
	}


	float forwardsProportion = glm::dot(glm::normalize(glm::vec4(velocity,1)), glm::normalize(currForwards));

	if (glm::isnan(forwardsProportion))
	{
		return;
	}

	float fixedProp =1-  (forwardsProportion + 1.0f) / 2.0f;

	glm::vec3 finalResistiveForce = -velocity * fixedProp * AIR_RESISTANCE_MULTIPLIER;

	//physics->applyForce(finalResistiveForce);
}

void RaceShip::draw(RenderSettings * rs)
{
	Object3D::draw(rs);
}

void RaceShip::turnLeft()
{
	physics->body->applyTorque(convert(glm::vec3(currRotMat * glm::vec4(0, 26000, 0, 1))));
}

void RaceShip::turnRight()
{
	physics->body->applyTorque(convert(glm::vec3(currRotMat * glm::vec4(0, -26000, 0, 1))));
}

void RaceShip::forceForward(float speed)
{
	physics->applyForce(currRotMat * glm::vec4(0, 0, speed, 1));
}

void RaceShip::forceBackward()
{
	physics->applyForce(currRotMat * glm::vec4(0, 0, -0.5f * forwardForce, 1));
}

void RaceShip::forceUp()
{
	physics->body->applyForce(btVector3(0, 30000, 0), btVector3(0, 0, 0));
}

void RaceShip::roll()
{
	physics->body->applyTorque(btVector3(0, 0, 20000));
}
