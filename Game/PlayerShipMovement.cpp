#include "PlayerShipMovement.h"
#include <Game.h>
#include "RaceShip.h"

PlayerShipMovement::PlayerShipMovement(RaceShip * newShip)
{
	gameRefr = Game::singleton();
	ship = newShip;
}


PlayerShipMovement::~PlayerShipMovement()
{
}

void PlayerShipMovement::setup()
{
}

void PlayerShipMovement::update(double deltaTime)
{
	if (!active) return;
	
	Keystate currKeys = gameRefr->getKeys();
	
	if (currKeys & Keys::Up)
	{
		ship->forceForward();
	}

	if (currKeys & Keys::Down)
	{
		ship->forceBackward();
	}

	if(currKeys & Keys::Right)
	{
		ship->turnRight();
	}

	if (currKeys & Keys::Left)
	{
		ship->turnLeft();
	}

	if (currKeys & Keys::Space)
	{
		ship->forceUp();
	}
}
