#pragma once
#include "MainMenu.h"
#include "main.h"
#include <EngineWrapper.h>
#include "World.h"
#include "TestScene.h"
#include "World2.h"

int main(int argc, char* argv[])
{
	EngineWrapper * horizonsInstance = new EngineWrapper();

	horizonsInstance->initGL(argc, argv , "WipeOut");

	horizonsInstance->sortConfig();

	Scene * levelOne = new MainMenu();
	horizonsInstance->initializeGame(levelOne);

	horizonsInstance->addScene(new World());
	horizonsInstance->addScene(new World2());
	horizonsInstance->addScene(new TestScene());


	horizonsInstance->startGame();

	return 0;
}