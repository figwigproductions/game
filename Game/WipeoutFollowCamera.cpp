#include "WipeoutFollowCamera.h"
#include <PhysicsHandler.h>
#include <Game.h>



WipeoutFollowCamera::WipeoutFollowCamera(GameObject* newPlayer)
{
	posTrackRelRotPlayer = glm::vec3(0.0f, 0.0f, 0.0f);
	posRelPlayer = glm::vec3(0.0f, 0.0f, -30.0f);
	target = newPlayer;

	cameraAngle = glm::vec2(0, 0);
}


WipeoutFollowCamera::~WipeoutFollowCamera()
{
}

void WipeoutFollowCamera::update(double delta, int mouseX, int mouseY, bool playing)
{
	float deltaTime = delta;

	cameraAngle.x -= (1.0f * (mouseY)) / mouseSens;
	cameraAngle.y -= (-1.0f * (glm::min(mouseX, 900))) / mouseSens;


	if (cameraAngle.y > M_PI * 2) cameraAngle.y -= M_PI * 2;
	if (cameraAngle.y < -M_PI * 2) cameraAngle.y += M_PI * 2;

	if (cameraAngle.x > maxX)
	{
		cameraAngle.x = maxX - 0.001f;
	}
	else if (cameraAngle.x < minX)
	{
		cameraAngle.x = minX + 0.001f;
	}

	///<Summary>
	///1. The first step calculates the new camera position
	///</Summary>
	glm::vec3 playerPos = glm::vec3(0);
	if (target != nullptr)
	{
		playerPos = target->getPos();
	}

	float s = -cameraAngle.y;
	float t = -cameraAngle.x;

	targetCameraPos = playerPos;

	targetCameraPos.z -= radius * cos(s) * sin(t);
	targetCameraPos.x -= radius * sin(s) * sin(t);
	targetCameraPos.y -= radius * cos(t);


	//cameraPos = cameraPos + ((maxDistPerSec / 1000.0f)*deltaTime)* targetCameraPos;
	cameraPos = targetCameraPos;

	///<Summary>
	///2. The second step calculates the look target
	///</Summary>

	cameraTarget = playerPos;


	///<Summary>
	///3. Calculate the camera matrix and mulitply it by the perspective matrix
	///</Summary>

	//Test if the camera  really should be above the ground even if it is currently below!
	glm::vec3 cameraTestPos = cameraPos;
	cameraTestPos.y += 5.0f;

	RayCastResult r = Game::singleton()->getPhysicsEngine()->castRayDown(cameraTestPos);

	//Is it a valid hit point?
	if (r.hitPos.y > -100.0f)
	{
		minHeightAtPoint = r.hitPos.y + 2.7f;
	}


	if (cameraPos.y < minHeightAtPoint)
	{
		cameraPos.y = minHeightAtPoint;
	}

	//Get the vector the camera is looking down
	cameraDirection = glm::normalize(cameraTarget - cameraPos);

	//work out what camera right is
	cameraRight = glm::normalize(glm::cross(up, cameraDirection));

	//work out what  camera up is
	cameraUp = glm::cross(cameraDirection, cameraRight);

	cameraNoPersp = glm::lookAt(cameraPos, cameraTarget, cameraUp);

	camera = persp * cameraNoPersp;

	///<Summary>
	///5. Decay the totalY and totalX rot values
	///</Summary>

	reportPosition();
}


glm::mat4 WipeoutFollowCamera::getLookAtMatrix(bool invertY, glm::vec3 pos)
{
	if (invertY)
	{

		//Use case - water reflections
		//pos is already lower, we just need to work out a new camera target

		glm::vec3 lookVector = cameraPos - cameraTarget;

		lookVector.y = -lookVector.y;


		return persp * glm::lookAt(pos, pos - lookVector, glm::vec3(0, 1, 0));
	}
	else
	{
		std::cout << ("THIS DOES NOT WORK YETz\n");
	}

}