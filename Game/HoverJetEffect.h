#pragma once
#include <Effect3D.h>
#include <random>

class HoverJetEffect : public Effect3D
{
public:
	HoverJetEffect();
	~HoverJetEffect();

protected:
	void generateCircle(int noPoints, float radius);
	void generateSecondaryCircle(int noPoints, float radius);
	void shaderUniformSetup(glm::mat4 camera);
	void sortVAO();
	void sortVAO2();


	bool firstCircle = true;

	GLuint VAO2;
	int totalPoints2 = 0;

	GLuint VBO = 0;
	GLuint VBO2 = 0;
	GLuint colorVBO = 0;
	GLuint colorVBO2 = 0;
	GLuint anglesVBO = 0;
	GLuint anglesVBO2 = 0;

	float* points;
	float* points2;
	float* colors;
	float* colors2;
	float* angles;
	float* angles2;

	float angle = 0.0f;
	float angleMod = 0.0f;
	float radius = 0.4f;

	float magnitude = 0.4f;

	std::default_random_engine randGenerator;
};

