#pragma once
#include <Scene.h>
#include <GameObject.h>
#include <Camera.h>
#include "WipeOutObjectFactory.h"
#include "KeyboardMove.h"
#include "PlayerShipMovement.h"
#include "RaceShip.h"

class World : public Scene
{
public:
	World();
	~World();


	void run();
	//Run HAS to set the playing state, and the player pointer
	void load(Game* thisGame);

	void loadInit();

	void loadTextures();
private:

	GameObject * editor;

	void loadWorld();

	void loadWater();

	void loadEditor();

	void loadCamera();

	void loadLighting();

	Camera * editorCamera = nullptr;
	Camera * playerCamera = nullptr;

	void loadShip();

	void loadAiShip();

	VAOData* shipData = nullptr;

	WipeOutObjectFactory* objectMaker = nullptr;
	RaceShip * ship;

	KeyboardMove* cubeKeyboardMove;
	PlayerShipMovement* shipKeyboardMove;
};

