#pragma once
#include <PrivateBehaviour.h>
#include "RaceShip.h"
#include <Path.h>

class RaceAI : public PrivateBehaviour
{
public:
	RaceAI(RaceShip * ship, Path * path);
	~RaceAI();

	void update(double);
private:
	RaceShip* raceShip =nullptr;
	Path* path = nullptr;
	Waypoint* currWaypoint = nullptr;

	int index = 0;

	float angleFudge = 0.05f;

	float forwardsThreshold = 0.15f;


	float minimumRadius = 5.0f;

	float forwardForce = 62500.0f;

	int crossLoc = 0;
	int headingLoc = 0;
	int distLoc = 0;
	int dotLoc = 0;
};

