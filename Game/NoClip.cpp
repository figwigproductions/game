#include "NoClip.h"

#include <Game.h>
#include <Camera.h>


NoClip::NoClip(PrivateBehaviour* c, PrivateBehaviour* f, Camera * ca, Camera * ca2) : beh1(c), beh2(f)
{
	beh1->setActive(true);
	beh2->setActive(false);
	cam = ca;
	cam2 = ca2;
	cam->setTarget(beh1->getTarget());
	cam2->setTarget(beh2->getTarget());
}


NoClip::~NoClip()
{
}


void NoClip::update(double delta)
{
	if (active)
	{
		Game* game = Game::singleton();
		if (game->getKeys() & Keys::x)
		{
			first = !first;
			if (first)
			{
				beh1->setActive(true);
				beh2->setActive(false);
				game->setCamera(cam);

			}
			else
			{
				beh2->setActive(true);
				beh1->setActive(false);
				game->setCamera(cam2);
			}

			ticker = 300;
		}
	}

	if (ticker > 0.1) {
		ticker -= delta; active = false;
	}
	else
	{
		active = true;
	}
}